/*
scrap allskills: https://www.d20pfsrd.com/skills/
function camalize(source) {
        return source
          .toLowerCase()
          .trim()
          .split(/[ \(\)]/g)
          .filter(Boolean)
          .reduce(
            (result, current) =>
              result + current[0].toUpperCase() + current.slice(1)
          );
      }
function toAbility(mod) {
    switch(mod){
    case "Str": return "Strength";
    case "Dex": return "Dexterity";
    case "Con": return "Constitution";
    case "Int": return "Intelligence";
    case "Wis": return "Wisdom";
    case "Cha": return "Charisma";
    default: return "";
    }
}

copy(Object.fromEntries(new Map([...document.querySelectorAll('.article-content > div:nth-child(5) > table:nth-child(1) > tbody:nth-child(3) > tr')].map(x => [[camalize(x.querySelector('td').textContent.trim())], {"trained": x.querySelector("td:nth-child(21)").textContent.trim() !== "Yes", "armorPenalty": x.querySelector("td:nth-child(22)").textContent.trim() === "Yes" ? -1 : 0, "ability": toAbility(x.querySelector("td:nth-child(23)").textContent.trim()), "value": 0 }]))))
*/

function sumSkill(
  key,
  classSkills,
  bonusSkills,
  raceSkills,
  ability,
  armorPenalty,
  skill
) {
  const canUse =
    classSkills[key] !== undefined ||
    (raceSkills && raceSkills[key] !== undefined) ||
    !skill.trained;
  if (!canUse) return { total: null, detail: "" };

  const totalClassBonus =
    ((classSkills[key] ?? 0) > 0 ? classSkills[key] + 3 : 0) +
    ((bonusSkills && bonusSkills[key]) ?? 0);
  const totalRaceBonus = (raceSkills && raceSkills[key]) ?? 0;
  const totalArmorPenalty = armorPenalty * (skill?.armorPenalty ?? 0);
  const totalAbilityBonus = canUse ? ability[skill.ability] : 0;
  return {
    total:
      totalClassBonus + totalRaceBonus + totalAbilityBonus + totalArmorPenalty,
    detail: `class: ${totalClassBonus} + race: ${totalRaceBonus} + ability: ${totalAbilityBonus} + armor: ${totalArmorPenalty}`,
  };
}

// sumSkill("a", {"a": 2},{"a": 2},{"w": 3}, 2, {"a": {armorPenalty:1, ability: "w"}})
function sumSkills(
  classSkills,
  bonusSkills,
  raceSkills,
  ability,
  armorPenalty
) {
  const skills = [];
  for (let key of Object.keys(allSkills)) {
    const { total, detail } = sumSkill(
      key,
      classSkills,
      bonusSkills,
      raceSkills,
      ability,
      armorPenalty,
      allSkills[key]
    );
    if (total === null) continue;

    skills.push({
      total: total,
      detail: detail,
      key: key,
      translation: translateSkill(key),
    });
  }
  return skills;
}

function loadQueryString() {
  const data = {
    vocation: "barbarian",
    race: "human",
    backgroundAbility: "commoner",
    title: "",
  };
  const search = window.location.search;
  if (!search) return data;

  const pair = Object.entries(charaterTemplates).find(([key, _]) =>
    search.toLowerCase().includes(key)
  );

  if (pair !== undefined) {
    const [_, character] = pair;
    if (character.backgroundAbility !== "custom") return character;
    background.custom.strength = character.custom.strength;
    background.custom.dexterity = character.custom.dexterity;
    background.custom.constitution = character.custom.constitution;
    background.custom.intelligence = character.custom.intelligence;
    background.custom.wisdom = character.custom.wisdom;
    background.custom.charisma = character.custom.charisma;
    return character;
  }

  const parameters = [...new URLSearchParams(search).entries()];
  if (parameters.length == 0) return data;

  const validParameters = parameters.filter(
    ([key, value]) => !!key && !!value && parseFloat(value).toString() === value
  );
  if (validParameters.length == 0) return data;

  const results = Object.fromEntries(
    validParameters.map(([key, value]) => [key, parseFloat(value)])
  );

  // for (const key of Object.keys(data).filter(key => results[key] != undefined)) {
  //   data[key] = results[key];
  // }

  data.backgroundAbility = "custom";
  background.custom.strength = results.strength || background.custom.strength;
  background.custom.dexterity =
    results.dexterity || background.custom.dexterity;
  background.custom.constitution =
    results.constitution || background.custom.constitution;
  background.custom.intelligence =
    results.intelligence || background.custom.intelligence;
  background.custom.wisdom = results.wisdom || background.custom.wisdom;
  background.custom.charisma = results.charisma || background.custom.charisma;

  return data;
}

function translateBackground(background) {
  return translation["fr"].backgrounds[background];
}

function translateSkill(skill) {
  return translation["fr"].skills[skill];
}

function translateAbility(ability) {
  return translation["fr"].abilities[ability];
}

function translateAlignment(alignment) {
  return translation["fr"].alignments[alignment];
}

function buildMeleeAttack(
  melee,
  attackAbility,
  bba,
  race,
  str,
  weaponAttackBonus
) {
  const name = melee.translation;
  const attack = attackAbility + bba + (race.attack ?? 0) + weaponAttackBonus;
  const damage = race.size === "P" ? melee.damageSmall : melee.damage;
  const strToDamage = str != 0 ? forceSymbol(str) : "";
  return `${name}, ${forceSymbol(attack)} (${damage}${strToDamage})`;
}

function buildRangedAttack(ranged, attackAbility, bba, race) {
  const name = ranged.translation;
  const attack = attackAbility + bba + (race.attack ?? 0);
  const damage = race.size === "P" ? ranged.damageSmall : ranged.damage;
  return `${name}, ${forceSymbol(attack)} (${damage})`;
}

// function buildNickname(raceKey, classKey) {
//   const vocation = classes[classKey];
//   debugger;
//   const race = races[raceKey];

//   if (this.title)
//     return this.title;

//   if (race.id && vocation.id)
//     return race.id + vocation.id;

//   return "Karaktero";
// }

new Vue({
  el: "#app",
  data: {
    translation: translation,
    abilities: abilities,
    alignments: alignments,
    levels: [1, 2, 3, 4],
    backgrounds: background,
    armorgroups: armorgroups,
    classes: classes,
    races: races,
    gears: gears,
    level: 1,
    alignment: "neutral",
    // what if strength is not available? i.e. changelin
    raceAbilityBonus: "strength",
    thumbnail: "",
    petThumbnail: "",
    custom: undefined,
    ...loadQueryString(),
    nickname: "",
  },
  computed: {},
  methods: {
    sumAbility: function (ability) {
      const baseAbility = background[this.backgroundAbility][ability];
      const level = ability.startsWith(this.classes[this.vocation].mainAbility)
        ? Math.floor(this.level / 4)
        : 0;
      if (
        this.races[this.race].hasBonusAbility &&
        this.raceAbilityBonus === ability
      ) {
        return baseAbility + level + 2;
      }

      return (
        baseAbility + level + (this.races[this.race].ability[ability] ?? 0)
      );
    },
    strength: function () {
      return this.sumAbility("strength");
    },
    dexterity: function () {
      return this.sumAbility("dexterity");
    },
    constitution: function () {
      return this.sumAbility("constitution");
    },
    intelligence: function () {
      return this.sumAbility("intelligence");
    },
    wisdom: function () {
      return this.sumAbility("wisdom");
    },
    charisma: function () {
      return this.sumAbility("charisma");
    },
    str: function () {
      return this.mod(this.strength());
    },
    dex: function () {
      return this.mod(this.dexterity());
    },
    con: function () {
      return this.mod(this.constitution());
    },
    int: function () {
      return this.mod(this.intelligence());
    },
    wis: function () {
      return this.mod(this.wisdom());
    },
    cha: function () {
      return this.mod(this.charisma());
    },
    mod: function (ability) {
      return Math.floor((ability - 10) / 2);
    },
    baseba: function () {
      return this.level === 1 || classes[this.vocation].levels === undefined
        ? classes[this.vocation].bba
        : classes[this.vocation].levels[this.level - 2].bba;
    },
    dmd: function () {
      return (
        10 +
        this.str() +
        this.dex() +
        this.baseba() +
        (races[this.race].dmd ?? 0) +
        (this.level === 1 || classes[this.vocation].levels === undefined
          ? classes[this.vocation].dmd ?? 0
          : classes[this.vocation].levels[this.level - 2].dmd ?? 0)
      );
    },
    bmo: function () {
      return this.str() + this.baseba() + (races[this.race].bmo ?? 0);
    },
    ac: function (armor, shield) {
      return (
        10 +
        this.dex() +
        (classes[this.vocation].acAbility === "wis" ? this.wis() : 0) +
        (this.custom?.armor ?? 0) +
        armor +
        shield +
        (classes[this.vocation].ac
          ?.map((x) => x.bonus)
          .reduce((a, b) => a + b, 0) ?? 0) +
        (races[this.race].ac?.map((x) => x.bonus).reduce((a, b) => a + b, 0) ??
          0)
      );
    },
    touch: function () {
      return (
        10 +
        this.dex() +
        (classes[this.vocation].acAbility === "wis" ? this.wis() : 0) +
        (classes[this.vocation].ac
          ?.filter((x) => x.type === undefined || x.type === "b")
          .map((x) => x.bonus)
          .reduce((a, b) => a + b, 0) ?? 0) +
        (races[this.race].ac
          ?.filter((x) => x.type === undefined || x.type === "b")
          .map((x) => x.bonus)
          .reduce((a, b) => a + b, 0) ?? 0)
      );
    },
    flatFooted: function (armor, shield) {
      return (
        10 +
        (this.custom?.armor ?? 0) +
        armor +
        shield +
        (classes[this.vocation].ac
          ?.filter((x) => x.type === undefined || x.type === "a")
          .map((x) => x.bonus)
          .reduce((a, b) => a + b, 0) ?? 0) +
        (races[this.race].ac
          ?.filter((x) => x.type === undefined || x.type === "a")
          .map((x) => x.bonus)
          .reduce((a, b) => a + b, 0) ?? 0)
      );
    },
    sumSave: function (ability, save) {
      return (
        (this.custom?.save && this.custom?.save[save]
          ? this.custom.save[save]
          : 0) +
        ability +
        (this.level === 1 || classes[this.vocation].levels === undefined
          ? classes[this.vocation].save[save]
          : classes[this.vocation].levels[this.level - 2].save[save]) +
        (races[this.race].save && races[this.race].save[save]
          ? races[this.race].save[save]
          : 0)
      );
    },
    fortitude: function () {
      return this.sumSave(this.con(), "fortitude");
    },
    reflex: function () {
      return this.sumSave(this.dex(), "reflex");
    },
    will: function () {
      return this.sumSave(this.wis(), "will");
    },
    hp: function (vocation) {
      return (
        this.con() * this.level +
        classes[vocation].hitDice +
        Math.ceil((classes[vocation].hitDice + 1) / 2) * (this.level - 1) +
        (races[this.race].hitPoint ?? 0)
      );
    },
    //   skillRanks: function () {
    //     return this.int() + classes[vocation].skillRanks;
    //   },
    skills: function () {
      const vocation = this.classes[this.vocation];
      const classSkills = this.custom?.skills ?? vocation.skills;

      const obj = Object.fromEntries(new Map(classSkills.map((s) => [s, 0])));
      const totalPoints =
        (this.int() +
          vocation.skillRanks +
          (this.races[this.race].skillPerLevel ?? 0)) *
        this.level;
      const classSkillsDealed = deal(obj, totalPoints);
      const ability = {
        strength: this.str(),
        dexterity: this.dex(),
        constitution: this.con(),
        intelligence: this.int(),
        wisdom: this.wis(),
        charisma: this.cha(),
      };
      const penalty =
        vocation.armor.armorPenalty + (vocation.shield?.armorPenalty ?? 0);
      return sumSkills(
        classSkillsDealed,
        addObject(vocation.bonusSkills ?? {}, this.custom?.bonusSkills ?? {}),
        this.races[this.race].skills,
        ability,
        penalty
      );
    },
    translateBackground: translateBackground,
    translateSkill: translateSkill,
    translateAbility: translateAbility,
    translateAlignment: translateAlignment,
    buildSkillHref: function (skill) {
      const skillWithoutParenthesis = skill.replace(/\(.*\)/g, "").trim();
      return `https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.${skillWithoutParenthesis}.ashx`;
    },
    forceSymbol: forceSymbol,
    languages: function () {
      if (this.custom?.languages) {
        return this.custom.languages;
      }
      const raceLanguages = flatDeep([this.races[this.race].language]);
      const int = this.int();
      return distinct(
        ["commun"]
          .concat(raceLanguages)
          .concat([this.classes[this.vocation].language])
          .concat(int > 0 ? [`+${int} langues supplémentaires`] : [])
          .filter(Boolean)
      );
    },
    dd: function (spellLevel) {
      return (
        10 +
        spellLevel +
        (this.classes[this.vocation].mainAbility
          ? this[this.classes[this.vocation].mainAbility]()
          : 0)
      );
    },
    abbr: function (term) {
      return abbreviations[term.toLowerCase()];
    },
    concentration: function () {
      return (
        this.level +
        (this.classes[this.vocation].mainAbility
          ? this[this.classes[this.vocation].mainAbility]()
          : 0)
      );
    },
    orderByKey: orderByKey,
    getRandomItem: getRandomItem,
    except: except,
    capitalize: capitalize,
    gear: function () {
      const index =
        indexOf(Object.keys(this.classes), this.vocation) +
        indexOf(Object.keys(this.races), this.race);
      const package = this.gears[index % this.gears.length];
      return this.classes[this.vocation].gear
        .concat(this.races[this.race].gear)
        .concat(package)
        .concat(this.custom?.gears ?? [])
        .filter(Boolean)
        .filter((x) => !(this.custom?.remove?.gears.includes(x) ?? false));
    },
    features: function () {
      const classFeatures = this.classes[this.vocation].features.slice(0);

      const features = this.races[this.race].features.concat(classFeatures);

      if (!this.classes[this.vocation].levels) {
        return features;
      }
      const levelFeatures = [];
      for (let i = 0; i < this.level - 1; i++) {
        if (this.classes[this.vocation].levels[i].features === undefined)
          continue;
        for (let feature of this.classes[this.vocation].levels[i].features) {
          levelFeatures.push(feature);
        }
      }
      return features.concat(levelFeatures);
    },
    feats: function () {
      if (this.custom?.feats) {
        return this.custom.feats;
      }

      const progressionFeats = Math.ceil(this.level / 2);

      const feats = [
        {
          name: `${progressionFeats} don${
            progressionFeats > 1 ? "s" : ""
          }  (niveau)`,
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Progression%20des%20personnages.ashx",
        },
      ]
        .concat(this.classes[this.vocation].feats ?? [])
        .concat(this.races[this.race].feats ?? []);

      if (!this.classes[this.vocation].levels) {
        return feats;
      }
      const levelFeats = [];
      for (let i = 0; i < this.level - 1; i++) {
        if (this.classes[this.vocation].levels[i].feats === undefined) continue;
        for (let feat of this.classes[this.vocation].levels[i].feats) {
          levelFeats.push(feat);
        }
      }
      return feats.concat(levelFeats);
    },
    traits: function () {
      if (this.custom?.traits) {
        return this.custom.traits;
      }

      return (this.classes[this.vocation].traits ?? []).concat(
        this.races[this.race].traits ?? []
      );
    },
    meleeAttack: function () {
      const str = this.str();
      const vocation = classes[this.vocation];
      const attackAbility = vocation.finesse ? this.dex() : str;
      const damageAbility =
        vocation.finesse && this.level === 3 ? this.dex() : str; // todo: feline grace should not be free with finess
      const race = races[this.race];
      const hasAttacks = Array.isArray(vocation.melee);
      let melees = hasAttacks ? vocation.melee : [vocation.melee];
      if (this.custom?.remove?.melee) {
        const nameToRemove =
          weapongroups.melee[this.custom.remove.melee].translation;
        console.log(nameToRemove);
        melees = melees.filter((m) => m.translation !== nameToRemove);
      }
      if (this.custom?.melee) {
        const customMelee =
          typeof this.custom.melee === "object"
            ? this.custom.melee
            : weapongroups.melee[this.custom.melee];
        melees = [customMelee].concat(melees);
      }
      const bba = this.baseba();
      const favoriteWeapon = this.custom?.favoriteWeapon ?? this.favoriteWeapon;
      return melees
        .map((melee) => {
          return buildMeleeAttack(
            melee,
            attackAbility,
            bba,
            race,
            !vocation.finesse && melee.hands == 2
              ? Math.floor(damageAbility * 1.5)
              : damageAbility,
            melee.name === favoriteWeapon ? 1 : 0
          );
        })
        .join(" ou ");
    },
    specialAttacks: function () {
      if (this.custom?.specialAttacks) {
        return this.custom.specialAttacks;
      }
      if (this.level === 1 || classes[this.vocation].levels === undefined) {
        return classes[this.vocation].specialAttacks ?? "";
      }
      return (
        classes[this.vocation].levels[this.level - 2]?.specialAttacks ?? ""
      );
    },
    rangedAttack: function () {
      const vocation = classes[this.vocation];
      const hasAttacks = Array.isArray(vocation.ranged);
      const attackAbility = this.dex();
      const race = races[this.race];
      const bba = this.baseba();
      return hasAttacks
        ? vocation.ranged
            .map((ranged) =>
              buildRangedAttack(ranged, attackAbility, bba, race)
            )
            .join(" ou ")
        : buildRangedAttack(vocation.ranged, attackAbility, bba, race);
    },
    initiative: function () {
      const raceBonus = races[this.race].initiative ?? 0;
      const classBonus = classes[this.vocation].initiative ?? 0;
      const initiative = this.custom?.initiative ?? 0;
      return this.forceSymbol(this.dex() + raceBonus + classBonus + initiative);
    },
    buildRarity: function () {
      const raceDescription = races[this.race].rarity;
      switch (raceDescription) {
        case rarities.Common:
          return `On ne peut ignorer les ${
            races[this.race].name
          }. Ils sont présents presque partout sur le continent`;

        case rarities.Uncommon:
          return `Il n'est pas rare de croiser la route des ${
            races[this.race].name
          }s. Il y en a dans toutes les villes.`;

        case rarities.Rare:
          return `Les ${
            races[this.race].name
          }s sortent rarement de leurs lieux de prédilection. Il est tout de même possible d'en croiser occasionnellement.`;

        case rarities.Legendary:
          return `Croiser la route des ${
            races[this.race].name
          }s n'est pas ordinaire. Il est rare d'en rencontrer.`;

        case rarities.Divine:
          return `Les ${
            races[this.race].name
          }s sont méconnus. Il arrive même que la réalité de leur existence soit questionnée. Pourtant vous êtes bien là.`;
        default:
          return "";
      }
    },
    buildNotes: function () {
      return this.notes ?? [];
    },
  },
});
