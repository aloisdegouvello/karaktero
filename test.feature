Feature: Build charaters

Scenario: Build Ygrune
Given the race is "Nain"
And the class is "Roublard"
And the alignement is "Neutre"
And the background is "Ingénieuse"
And the name is "Ygrune"
When I scroll to the character sheet
Then the name is "Ygrune"
# General
And the race is "Nain"
And the class is "Dwarf"
And the level is "1"
And the size is "M"
And the alignment is "Neutre"
And the initiative bonus is "+2"
# Defense
And the armor class is 14
And the touched armor class is 12
And the flat-footed armor class is 12
And the hit points is 12
And the reflex is "+4"
And the fortitude is "+1"
And the will is "+3"
# Attack
And the melee weapon is a "dague"
And the melee attack is "+2"
And the melee damage is "1d4"
And the special attack is "attaque sournoise +1d6"
But the class ".BDsorts" does not not exist
# Ability
And the strength is 10
And the strength mod is "+0"
And the dexterity is 14
And the dexterity mod is "+2"
And the constitution is 12
And the constitution mod is "+1"
And the intelligence is 14
And the intelligence mod is "+2"
And the wisdom is 16
And the wisdom mod is "+3"
And the charisma is 12
And the charisma mod is "+1"
# Fight
And the bab is "+0"
And the bmo is "+0"
And the dmd is 12
# Misc
And skills are "Acrobaties +6 , Artisanat +2 , Bluff +7 , Connaissances (exploration souterraine) +2 , Connaissances (folklore local) +6 , Diplomatie +3 , Discrétion +6 , Déguisement +5 , Escalade +4 , Escamotage +6 , Estimation +2 , Evasion +6 , Intimidation +1 , Linguistique +2 , Natation +0 , Perception +7 , Premier secours +3 , Profession +3 , Psychologie +5 , Représentation +1 , Sabotage +6 , Survie +3 , Utilisation d'objets magiques +1 , Vol +2 , Équitation +2"
And languages are "commun, nain, +2 langues supplémentaires"
And features are "vision dans le noir, sens des pièges +1"
And geats starts with "armure de cuir, outils de voleur,"