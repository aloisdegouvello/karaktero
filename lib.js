function compare(a, b, key) {
  if (a[key] < b[key]) {
    return -1;
  }
  if (a[key] > b[key]) {
    return 1;
  }
  return 0;
}

function orderByKey(arr, key) {
  let tmp = [...arr];
  tmp.sort((a, b) => compare(a, b, key));
  return tmp;
}

function getRandomItem(items) {
  return items[(items.length * Math.random()) | 0];
}

function roll3d6() {
  const rolls = [
    Math.floor(Math.random() * (6 - 1 + 1) + 1),
    Math.floor(Math.random() * (6 - 1 + 1) + 1),
    Math.floor(Math.random() * (6 - 1 + 1) + 1),
    Math.floor(Math.random() * (6 - 1 + 1) + 1),
  ];
  return rolls.reduce((a, b) => a + b) - Math.min.apply(null, rolls);
}

function camalize(source) {
  return source
    .toLowerCase()
    .trim()
    .split(/[ \(\)]/g)
    .filter(Boolean)
    .reduce(
      (result, current) => result + current[0].toUpperCase() + current.slice(1)
    );
}

// https://tio.run/##pdG9bsMgEADg2TzFLZWM7CL/9EdK6hfo0AwdowyYYBfXgsomlSLLz@5CVKlVBoOd5RBwfHc6GvpNe9aJL30v1ZFPU3WSTAsl4chpG6qyiUHiAQUt16CggIEQYk5HFDAlew2f/Nyb413ZcKaJ3dlHeIuCSnUQ2mfC3Cdbs7yANEsUYTBgoPY2ey/g7oKQlstafxwOEBWQosBU6Lg@dRIUGhFCtpxqOWlVHb6@795Irzsha1Gdw0urA90kYwwJxlAUBVylDEA3kIwY@zrpjJNaxwuKS3dTMZgk8G3tV5xtb5WYucR0qZjPiNmf6E3GzG@YNjITrQ5Ldedg/@tLceeMbVyL5554ugZ/cP3lLfijE8/W40@eeLYGf57B82t8mn4A
// https://tio.run/##pdHPT4MwFAfwM/wV72JCM6zA/JFscjLxoNEdPC47sK5snU1roDNZCH87trgJMQoduxRaXj/vm8c2@UxykrEPdSnkilZVuhNEMSlgRRPuyeXWB4EK1@FUgYQYCoyxPi1dh0iRK3in@1wfz5ZbShQ2O3MJTY/fNwlPHyjjuuYlURtM9Lsn4AoiU5PKDDxDM/09mOrH/c8NvRuNEOjejpwbeM7gou6HORVrtVksYBRD6Dql2@72yKVWD@1Ss2n6NVXP37lbRS25Lv8/Xd3h73hHupXNyajaZQKkq3O6JoHkFHO59p7eZq84VxkTa5buvXriRTIJSh8ChCCOY/hVUkAygaBEyNYJO5zQOFaQv@wP5YMuAttoB7Ez3iAx6hPDU8Vxhxg1ojXpE7thmpXo1ehwqt472LZ@Kt47Y7MOxceWeDgEv@77l@fgN714NBy/tcSjIfhdBz4@C/dXxwaoqr4A
function deal(obj, n) {
  let o = { ...obj };
  const keys = Object.keys(obj);
  const halfCeil = Math.ceil(n / 2);
  for (let i = 0; i < halfCeil; i++) {
    o[keys[i % keys.length]] += 1;
  }

  const halfFloor = Math.floor(n / 2);
  const halfKeys = Math.round(keys.length / 2);
  for (let i = 0; i < halfFloor; i++) {
    o[keys[i % halfKeys]] += 1;
  }
  return o;
}

function except(a, b) {
  const setA = new Set(a);
  const setB = new Set(b);
  return [...setA].filter((x) => !setB.has(x));
}

function capitalize(source) {
  return source[0].toUpperCase() + source.slice(1);
}

function flatDeep(arr, d = Infinity) {
  return d > 0
    ? arr.reduce(
        (acc, val) =>
          acc.concat(Array.isArray(val) ? flatDeep(val, d - 1) : val),
        []
      )
    : arr.slice();
}

function distinct(source) {
  return [...new Set(source)];
}

// did I recode findIndex? I think so.
function indexOf(items, item) {
  for (let i = 0; i < items.length; i++) {
    if (items[i] === item) {
      return i;
    }
  }
  return -1;
}

function forceSymbol(number) {
  return number >= 0 ? `+${number}` : number.toString();
}

function addObject(object1, object2) {
  const a = JSON.parse(JSON.stringify(object1));
  const b = JSON.parse(JSON.stringify(object2));
  return Object.assign(
    Object.fromEntries(
      Object.entries(a).map(([k, v]) => {
        if (!b.hasOwnProperty(k)) {
          return [k, v];
        }
        let tmp = b[k];
        delete b[k];
        return [k, v + tmp];
      })
    ),
    b
  );
}
