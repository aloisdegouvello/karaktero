const charaterTemplates = {
  katell: {
    vocation: "wizard",
    race: "elf",
    backgroundAbility: "scholar",
    title: "Asara",
  },
  mayne: {
    vocation: "rogue",
    race: "tiefling",
    backgroundAbility: "custom",
    title: "Bi Ryung",
    level: 3,
    custom: {
      strength: 10,
      dexterity: 14,
      constitution: 14,
      intelligence: 8,
      wisdom: 12,
      charisma: 16,
      traits: [
        {
          name: "Magical Knack",
          link: "https://www.d20pfsrd.com/traits/magic-traits/magical-knack/",
        },
        {
          name: "Blade of the Society",
          link: "https://www.d20pfsrd.com/traits/combat-traits/blade-of-the-society-rogue-pathfinder-society/",
        },
      ],
      feats: [
        // https://www.d20pfsrd.com/feats/combat-feats/weapon-focus-combat--
        // https://www.d20pfsrd.com/feats/combat-feats/sap-adept-combat
        // {
        //   name: "Bookish Rogue",
        //   link: "https://www.d20pfsrd.com/feats/general-feats/bookish-rogue/",
        // },
        // Accomplished Sneak Attacker
        {
          name: "Combat à deux armes",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Combat%20%c3%a0%20deux%20armes.ashx",
          description:
            "Le personnage sait se battre avec une arme dans chaque main. À chaque round, il peut faire une attaque supplémentaire avec son arme secondaire.",
        },
      ],
      specialAttacks: "attaque sournoise 1d4+2d8+1 si dague sinon +1d4+1",
      gears: [
        "masque",
        {
          name: "Gants de protection +1",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Bracelets%20darmure.ashx",
        },
        {
          name: "Potion d'invisibilité",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Invisibilit%c3%a9.ashx",
        },
        // {
        //   name: "Amulette de bois représentant un chat fabriquée par Magdeline (DEX +2)",
        //   link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Ceinturon%20de%20Dext%C3%A9rit%C3%A9%20du%20Chat.ashx"
        // }
      ],
      armor: 1,
      languages: ["commun"],
      remove: {
        gears: ["rations de voyage (4 jours)"],
      },
    },
    thumbnail: "https://i.ibb.co/1r63ZTj/biryung.jpg",
    notes: [
      {
        name: "Archétype: Maître des couteaux",
        link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Ma%c3%aetre%20des%20couteaux%20(roublard).ashx",
        description:
          "Perte de recherche des pièges, bonus escamotage, d8 au lieu de d6 pour les dagues",
      },
      {
        name: "Talent: Magie Mineure (Manipulation à distance 3/j)",
        link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Talents.ashx#MAGIEMINEURE",
        description: "Télékinésie limitée (2,5 kg max.)",
      },
      {
        name: "Attaque spéciale: dagues, +1 (1d4) et +1 (1d4)",
        description:
          "Le personnage sait se battre avec une arme dans chaque main. À chaque round, il peut faire une attaque supplémentaire avec son arme secondaire.",
        link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Actions%20sp%c3%a9ciales.ashx#COMBAT2ARMES",
      },
      {
        name: "Attaque spéciale sournoise: dagues, +1 (1d4+2d8+1) et +1 (1d4+2d8+1)",
        description:
          "Le personnage sait se battre avec une arme dans chaque main. À chaque round, il peut faire une attaque supplémentaire avec son arme secondaire.",
        link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Actions%20sp%c3%a9ciales.ashx#COMBAT2ARMES",
      },
    ],
  },
  enora: {
    vocation: "druid",
    race: "orc",
    backgroundAbility: "rustic",
    title: "Draïna",
    level: "3",
    custom: {
      initiative: 4,
      feats: [
        {
          name: "Science de l'initiative",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Science%20de%20linitiative.ashx",
          description:
            "Le personnage bénéficie d’un bonus de +4 au test d’initiative.",
        },
        {
          name: "Robustesse",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Robustesse.ashx",
        },
      ],
    },
    petThumbnail: "https://i.ibb.co/gyBRx7d/buck2.png",
    thumbnail: "https://i.ibb.co/P5dBmW8/enora-square.png",
  },
  ygrune: {
    vocation: "rogue",
    race: "dwarf",
    backgroundAbility: "resourceful",
    title: "Ygrune",
    level: 2,
  },
  magnus: {
    vocation: "warpriest",
    race: "human",
    backgroundAbility: "custom",
    title: "Magnus Kadgar",
    level: 2,
    custom: {
      strength: 14,
      dexterity: 10,
      constitution: 14,
      intelligence: 10,
      wisdom: 14,
      charisma: 14,
      feats: [
        {
          name: "Attaque en puissance",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Attaque%20en%20puissance.ashx",
          description:
            "Le personnage est capable de réaliser des attaques de mêlée mortelles en sacrifiant de la précision contre de la puissance.",
        },
        {
          name: "Bénédiction accélérée",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.B%c3%a9n%c3%a9diction%20acc%c3%a9l%c3%a9r%c3%a9e.ashx",
          description:
            "Le personnage peut octroyer ses bénédictions plus rapidement.",
        },
      ],
      languages: ["commun", "nain"],
      traits: [
        {
          name: "Seconde Chance",
          link: "https://www.d20pfsrd.com/traits/religion-traits/lessons-of-chaldira-chaldira-zuzaristan/",
        },
        {
          name: "Forgeron",
          link: "https://www.d20pfsrd.com/traits/campaign-traits/curse-of-the-crimson-throne/love-lost/",
        },
      ],
      bonusSkills: {
        craft: 2,
      },
      save: {
        will: -1,
      },
      melee: "lucerneHammer",
      favoriteWeapon: "lucerneHammer",
      skills: [
        "craft",
        "knowledgeReligion",
        "climb",
        "senseMotive",
        "diplomacy",
        "intimidate",
        "ride",
        "heal",
        "knowledgeEngineering",
        "spellcraft",
        "survival",
        "handleAnimal",
        "profession",
        "swim",
      ],
      remove: {
        melee: "heavyMace",
        gears: ["rations de voyage (4 jours)"],
      },
      divinity: "Ygrüne",
      gears: ["3 parchemins d'identification des propriétés magiques"],
    },
    notes: [
      {
        name: "Bénédiction de la terre",
        link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.B%c3%a9n%c3%a9diction%20de%20la%20terre.ashx",
      },
      {
        name: "Bénédiction des runes",
        link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.B%c3%a9n%c3%a9diction%20des%20runes.ashx",
      },
      {
        name: "Attaque en puissance: Marteau de Lucerne, +4 (1d12+7)", // arme de prédilection ignorée.
        link: "https://brutal.histographistes.com/?bba=1&strength=16&powerAttackPenalty=-1&rageBonus=0&handle=1.5&weaponAttack=1",
      },
    ],
    thumbnail: "https://i.ibb.co/GTg4tHR/magnus.png",
  },
  ryl: {
    vocation: "sorcerer",
    race: "halfDrow",
    backgroundAbility: "custom",
    title: "Ryl",
    custom: {
      strength: 8,
      dexterity: 16,
      constitution: 12,
      intelligence: 12,
      wisdom: 16,
      charisma: 8,
      gears: [
        // {
        //   name: "Amulette de bois représentant un hibou fabriquée par Magdeline (INT +2)",
        //   link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Bandeau%20dInspiration.ashx"
        // }
      ],
    },
    thumbnail: "https://i.ibb.co/Mfp5Mj3/Ryl-2-square-small.jpg",
  },
  morgwen: {
    vocation: "witch",
    race: "aasimar",
    backgroundAbility: "custom",
    title: "Morgwen Kyteler",
    custom: {
      strength: 8,
      dexterity: 14,
      constitution: 12,
      intelligence: 18,
      wisdom: 10,
      charisma: 8,
      feats: [
        {
          name: "Écriture de parchemins",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.%c3%89criture%20de%20parchemins.ashx",
          description: "Le personnage fabrique des parchemins magiques.",
        },
        {
          
          name: "(à faire)",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.%c3%89criture%20de%20parchemins.ashx",
          description: "Le personnage fabrique des parchemins magiques.",
        
        }
      ],
      languages: [
        "commun",
        "céleste",
        "aquatique",
        "aérien",
        "sylvestre",
        "draconien",
      ],
      bonusSkills: {
        fly: 3,
      },
      gears: [
        "pochons en tissus",
        "3 livres",
        {
          name: "2 doses de thé divinatoire",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Th%c3%a9%20Divinatoire.ashx",
        },
        // {
        //   name: "Amulette de bois représentant un renard ou corbeau fabriquée par Magdeline (INT +2)",
        //   link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Bandeau%20dIntelligence.ashx"
        // }
      ],
      remove: {
        gears: ["rations de voyage (4 jours)"],
      },
    },
    notes: [
      "Aasimar d'ascendence humaine",
      {
        name: "Maléfice: Lien de paix",
        link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Mal%c3%a9fices.ashx#Liendepaix",
        description:
          "Empêche une cible unique de dégainer ses armes pour un nombre de rounds égal au niveau de la sorcière.",
      },
      {
        name: "Fascination pour les livres",
        link: "https://discord.com/channels/@me/978604074649923635/982640456246644808",
        description:
          "fascination pour les livres... du coup si elle en voit un, elle risque d'aller le regarder sans même demander l'autorisation ou calculer quelqu'un qui lui parlerait en même temps",
      },
      {
        name: "Scrat, l'écureuil volant",
        link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.%c3%89cureuil%20volant.ashx",
      },
      "livre: Le Beau Bouquin des Bêtes par Bella Barl",
      "livre: Mythes et légendes des Elfes par Maître Velimir Veldal",
      "livre: L'histoire de Hugh le Haut par Gimi Jim",
    ],
    level: 3,
    thumbnail: "https://i.ibb.co/JcBfN5m/morgwen2.jpg",
  },
};
