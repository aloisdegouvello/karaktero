const rogueLevels = [
  {
    save: {
      fortitude: 0,
      reflex: 3,
      will: 0,
    },
    bba: 1,
    features: [
      {
        name: "esquive totale",
        link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Roublard.ashx#Esquive_totale_Ext_3",
      },
      "talent de roublard",
    ],
    specialAttacks: "attaque sournoise +1d6+1",
  },
  {
    save: {
      fortitude: 1,
      reflex: 3,
      will: 1,
    },
    bba: 2,
    features: ["sens du danger +1"],
    specialAttacks: "attaque sournoise +2d6+1",
  },
  {
    save: {
      fortitude: 1,
      reflex: 4,
      will: 1,
    },
    bba: 3,
    features: ["esquive instinctive", "talent de roublard"],
    specialAttacks: "attaque sournoise +2d6+1",
  },
];

const warpriestLevels = [
  {
    save: {
      fortitude: 3,
      reflex: 0,
      will: 3,
    },
    bba: 1,
    specialAttacks: "arme sacrée 1d6, ferveur 1d6",
    spells: {
      level0: [
        {
          name: "diplomatie améliorée",
          description: "+2 à un unique test de Diplomatie ou d'Intimidation.",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Diplomatie%20am%c3%a9lior%c3%a9e.ashx",
        },
      ],
      level1: [
        {
          name: "bénédiction",
          description:
            "Les alliés gagnent +1 à l'attaque et aux jets de sauvegarde contre la terreur.",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.B%c3%a9n%c3%a9diction.ashx",
        },
      ],
    },
  },
  {
    save: {
      fortitude: 3,
      reflex: 1,
      will: 3,
    },
    bba: 2,
    features: [
      {
        name: "ferveur",
        link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Pr%c3%aatre%20combattant.ashx#FERVEUR",
      },
    ],
    feats: ["don supplémentaire (prêtre combattant)"],
    specialAttacks: "arme sacrée 1d6, ferveur 1d6",
    spells: {
      level1: [
        {
          name: "hostilité forcée",
          description:
            "Oblige les adversaires à attaquer le personnage au lieu de ses alliés.",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Hostilit%c3%a9%20forc%c3%a9e.ashx",
        },
      ],
    },
  },
  {
    save: {
      fortitude: 4,
      reflex: 1,
      will: 4,
    },
    bba: 3,
    features: ["Arme sacrée +1", "canalisation d'énergie"],
    specialAttacks: "arme sacrée 1d6, ferveur 1d6",
    spells: {
      level2: [
        {
          name: "protection d'autrui",
          description: "Le PJ subit 1/2 dégâts à la place du sujet",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Protection%20dautrui.ashx",
        },
      ],
    },
  },
];

const witchLevels = [
  {
    save: {
      fortitude: 0,
      reflex: 0,
      will: 3,
    },
    bba: 1,
    features: [
      {
        name: "maléfice",
        link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Mal%c3%a9fices.ashx",
      },
    ],
    spells: {
      level0: [
        {
          name: "réparation",
          description: "Répare sommairement un objet.",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.R%c3%a9paration.ashx",
        },
      ],
      level1: [
        {
          name: "armure de mage",
          description: "Confère un bonus d'armure de +4.",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Armure%20de%20mage.ashx",
        },
      ],
    },
  },
  {
    save: {
      fortitude: 1,
      reflex: 1,
      will: 3,
    },
    bba: 1,
    features: ["maléfice"],
    spells: {
      level2: [
        {
          name: "hébétement de monstre",
          description:
            "Une créature vivante de 6 DV ou moins perd sa prochaine action.",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.H%c3%a9b%c3%a9tement%20de%20monstre.ashx",
        },
      ],
    },
  },
  {
    save: {
      fortitude: 1,
      reflex: 1,
      will: 4,
    },
    bba: 2,
    features: ["maléfice"],
    spells: {
      level1: [
        {
          name: "brume de dissimulation",
          description: "Le PJ est entouré de brouillard.",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Brume%20de%20dissimulation.ashx",
        },
      ],
      level2: [
        {
          name: "toile d'araignée",
          description: "Toiles gluantes dans un rayon de 6 m.",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Toile%20daraign%c3%a9e.ashx",
          save: "Ref",
        },
      ],
    },
  },
];

const druidLevels = [
  {
    save: {
      fortitude: 3,
      reflex: 0,
      will: 3,
    },
    bba: 1,
    features: [
      {
        name: "Déplacement facilité",
        link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.druide.ashx#DEPLACEMENTFACILITE",
      },
    ],
    spells: {
      level0: [
        {
          name: "stabilisation",
          description: "Stabilise une créature agonisante.",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Stabilisation.ashx",
        },
      ],
      level1: [
        {
          name: "enchevêtrement",
          description: "La végétation immobilise tout dans un rayon de 12 m.",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Enchev%c3%aatrement.ashx",
        },
      ],
    },
  },
  {
    save: {
      fortitude: 3,
      reflex: 1,
      will: 3,
    },
    bba: 2,
    features: [
      {
        name: "Absence de traces",
        link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.druide.ashx#ABSENCEDETRACES",
      },
    ],
    spells: {
      level2: [
        {
          name: "appel de la foudre",
          description: "Appelle la foudre (3d6 points de dégâts par éclair).",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Appel%20de%20la%20foudre.ashx",
        },
        {
          name: "peau d'écorce",
          description: "Confère un bonus d'armure naturelle de +2 (ou plus).",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Peau%20d%c3%a9corce.ashx",
        },
      ],
    },
  },
  {
    save: {
      fortitude: 4,
      reflex: 1,
      will: 4,
    },
    bba: 3,
    features: [
      {
        name: "forme animale (1/jour)",
        link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.druide.ashx#FORMEANIMALE",
      },
      {
        name: "résistance à l'appel de la nature",
        link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.druide.ashx#RESISTANCEALAPPELDELANATURE",
      },
    ],
    spells: {
      level1: [
        {
          name: "bouclier d'onde",
          description: "L’eau amortit une attaque ou un effet de feu.",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Bouclier%20donde.ashx",
        },
      ],
      level2: [
        {
          name: "feu de camp abrité ",
          description: "Crée un abri autour d’un feu de camp.",
          link: "https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Feu%20de%20camp%20abrit%c3%a9.ashx",
        },
      ],
    },
  },
];

const brawler = [
  {
    save: {
      fortitude: 3,
      will: 1,
      reflex: 3,
    },
    bba: 2,
    features: ["Déluge de coup du lutteur (combat à deux armes)"],
    feats: ["don supplémentaire (lutteur)"],
  },
  {
    save: {
      fortitude: 3,
      reflex: 3,
      will: 2,
    },
    bba: 3,
    features: ["Entraînement aux manœuvres 1"],
  },
  {
    save: {
      fortitude: 4,
      reflex: 4,
      will: 2,
    },
    bba: 4,
    features: ["KO 1/jour", "combat à mains nues (1d8)"],
    ac: [
      {
        name: "armure",
        bonus: 1,
        type: "b",
      },
    ],
    dmd: 1
  },
];
